// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>

#include <unistd.h>
#include <fcntl.h>

#include <fstream>
#include "Mundo.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <iostream>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	close(fd); // cerrar la tuberia de comunicación interprocesos
	
	munmap(pDatos, sizeof(Datos));  // desproyección del fichero
	unlink("DatosMemCompartida");
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();

	for(int i=0; i<esferas.size(); i++)
		esferas[i]->Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	float distancia = 100;
	int numero = 0;
	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	for(int i=0; i<esferas.size(); i++)
		esferas[i]->Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
		for(int j=0; j<esferas.size(); j++)
		{
			paredes[i].Rebota(*(esferas[j]));
			paredes[i].Rebota(jugador1);
			paredes[i].Rebota(jugador2);
		}

	for(int j=0; j<esferas.size(); j++){
		float dist = esferas[j]->centro.x - esferas[j]->radio;
		if(dist <= 0){
			dist += abs(jugador1.x1);
			dist  = abs(dist);
		}else{
			dist  = esferas[j]->centro.x + esferas[j]->radio - jugador1.x1;
			dist  = abs(dist); 
		}
		if(dist < distancia && esferas[j]->velocidad.x < 0){
			numero = j;
			distancia = dist;
		}
	}

	for(int i=0; i<esferas.size(); i++){
		if(jugador1.Rebota(*(esferas[i])))
			esferas[i]->contador+=1;
		if(jugador2.Rebota(*(esferas[i])))
			esferas[i]->contador+=1;
	}
	for(int i=0; i<esferas.size(); i++){
		if(esferas[i]->contador >= 5){
		esferas[i]->radio = 2*esferas[i]->radio/3;
		esferas[i]->contador = 0;
		if(esferas.size() < 3)
			esferas.push_back(new Esfera);
		if(esferas[i]->radio < 0.2f){
			delete esferas[i];
			esferas.erase(esferas.begin() + i);
		}
		}
	}
	
	for(int i=0; i<esferas.size(); i++)
		if(fondo_izq.Rebota(*(esferas[i])))
	{
		esferas[i]->centro.x=0;
		esferas[i]->centro.y=rand()/(float)RAND_MAX;
		esferas[i]->velocidad.x=2+2*rand()/(float)RAND_MAX;
		esferas[i]->velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		
		// Gestion del marcador cuando se registra un gol
		puntos[0] = puntos1;	// Puntos del jugador 1
		puntos[1] = puntos2;	// Puntos del jugador 2
		puntos[2] = 2;		// Jugador '2' marca punto
		
		// Escritura en la tuberia con nombre
		write(fd, puntos, sizeof(int)*3);
	}

	for(int i=0; i<esferas.size(); i++)
		if(fondo_dcho.Rebota(*(esferas[i])))
	{
		esferas[i]->centro.x=0;
		esferas[i]->centro.y=rand()/(float)RAND_MAX;
		esferas[i]->velocidad.x=2+2*rand()/(float)RAND_MAX;
		esferas[i]->velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos1++;
		
		puntos[0] = puntos1;	// Puntos del jugador 1
		puntos[1] = puntos2;	// Puntos del jugador 2
		puntos[2] = 1;		// Jugador '2' marca punto
		
		// Escritura en la tuberia con nombre
		write(fd, puntos, sizeof(int)*3);
	}
	
	// Se ejecuta la función End() cuando un jugador consigue 3 puntos
	pDatos->raqueta1 = jugador1;
	pDatos->esfera = *(esferas[numero]);
	
	if(pDatos->accion == 1)
		OnKeyboardDown('w',0,0);
	if(pDatos->accion == -1)
		OnKeyboardDown('s',0,0);
		
	if(puntos1 >= 3)
		End(true, 1);
	else if(puntos2 >= 3)
		End(true, 2);
	else 
		End(false, 0);
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
}

void CMundo::Init()
{
	Plano p;
	esferas.push_back(new Esfera);
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
// Se abre la tuberia con nombre para la comunicación, modo ESCRITURA
	fd = open("tuberia", O_WRONLY);
	if(fd < 0)
		perror("ERROR APERTURA 'tuberia' PARA ESCRITURA");  
		
// Proyección en memoria mmap para el movimiento del bot
	Datos.accion = 0;
	Datos.esfera = *(esferas[0]);
	Datos.raqueta1 = jugador1;
	
	fd1 = open("DatosMemCompartida", O_RDWR | O_TRUNC | O_CREAT, 0666);
	if(fd1 < 0)
		perror("ERROR APERTURA 'DatosMemCompartida' para LECTURA/ESCRITURA");		
	write(fd1, &Datos, sizeof(Datos));
	
	pDatos = static_cast<DatosMemCompartida*>(mmap(NULL,sizeof(Datos),PROT_READ | PROT_WRITE,MAP_SHARED,fd1,0));
	if(pDatos == MAP_FAILED){
		perror("ERROR al proyectar en memoria con 'mmap'");
		close(fd1);
		return;
	}
	close(fd1);
}

void CMundo::End(bool v, int p){
	if(v==true){
		printf("Victoria, jugador %d ha llegado a 3 puntos!!!\n", p);
		exit(1);
	}
}
