#include "DatosMemCompartida.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdlib.h>

int main(void){
	DatosMemCompartida* pDatos;
	struct stat bstat;
	int fd;
	
	fd = open("DatosMemCompartida", O_RDWR);
	if(fd<0){
		perror("ERROR APERTURA DEL FICHERO 'bot'");
		return 1;
	}
	pDatos = static_cast<DatosMemCompartida*>(mmap(0,sizeof(DatosMemCompartida),PROT_WRITE|PROT_READ,MAP_SHARED,fd,0));
	if(pDatos == MAP_FAILED){
		perror("ERROR EN LA PROYECCION DEL FICHERO 'bot'");
		close(fd);
		return 1;
	}
	close(fd);
	while(1){
		float distcentro = pDatos->raqueta1.y1 - 1.0f;
		
		if(distcentro < pDatos->esfera.centro.y)
			pDatos->accion = 1;
			
		else if(distcentro > pDatos->esfera.centro.y)
			pDatos->accion = -1;
			
		else if(distcentro == pDatos->esfera.centro.y)
			pDatos->accion = 0;
			
		usleep(25000);
	}

	munmap(pDatos, sizeof(DatosMemCompartida));
	unlink("DatosMemCompartida");
}
