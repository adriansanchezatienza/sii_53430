#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string>
#include <stdlib.h>

int main(void){
// Declaracion de variables y descriptores de ficheros
  	int fd;
	int puntos[3];
	
// Creacion de la tuberia con nombre 'tuberia'
	if(mkfifo("tuberia", 0777)<0){
		perror("No se ha podido crear la tuberia");
		return 0;
	}
	
// Abrir la tuberia con nombre para lectura
	if((fd = open("tuberia", O_RDONLY))<0){
		perror("No se ha podido leer de la tuberia");
		return 1;
	}

// Si se lee de la tuberia con éxito:	
	while(read(fd, puntos, sizeof(int)*3) == 3*sizeof(int)){
		if(puntos[2] == 1)
			printf("Jugador 1 marca 1 punto, lleva un total de %d puntos.\n", puntos[0]);
		else if(puntos[2] == 2)
			printf("Jugador 2 marca 1 punto, lleva un total de %d puntos.\n", puntos[1]);			
	}
	
// Se cierra la tuberia y se hace el unlink del fichero creado
	close(fd);
	unlink("tuberia");
	return 0;
}
